﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starcounter;

namespace OAuth2.Helpers
{
    public static class Extensions
    {
        public static void AddRange<T, S>(this IDictionary<T, S> target, IDictionary<T, S> source)
        {
            if (source == null)
            {
                throw new ArgumentNullException("Collection is null");
            }

            foreach (var item in source)
            {
                target[item.Key] = item.Value;
            }
        }
        

        public static string MakeAbsoluteUrl(this Request request, string url, params string [] p)
        {
            // TODO: Need a way to safely identify the url back to our self including the scheme
            // as for now, we assume we're running plain http://
            return String.Format("http://{0}{1}", request.Host, String.Format(url,p));
        }
    }
}
