﻿using System;
using System.Linq;
using System.Text;

using System.Security.Cryptography;
using System.Web;

using Starcounter;
using Simplified.Ring1;
using Simplified.Ring2;
using Simplified.Ring3;
using Simplified.Ring5;

using OAuth2.Core.Database;
using OAuth2.Core.Models;

namespace OAuth2.Helpers
{
    public class UserHelper
    {
        public static SystemUserSession GetCurrentSystemUserSession()
        {
            return Db.SQL<SystemUserSession>("SELECT o FROM Simplified.Ring5.SystemUserSession o WHERE o.SessionIdString = ?", Session.Current.SessionId).First;
        }

        public static SystemUserSession SignIn(SystemUser SystemUser)
        {
            SystemUserSession session = null;

            Db.Transact(() =>
            {
                // Create system user token
                SystemUserTokenKey token = new SystemUserTokenKey();

                token.Created = token.LastUsed = DateTime.UtcNow;
                token.Expires= DateTime.UtcNow.AddDays(7); // wanted to use 'SystemUser.TokenValidDays' but couldnt due to access level (and didnt want to resort to reflection stunts either)
                token.Token = CreateAuthToken(SystemUser.Username);
                token.User = SystemUser;
                session = AssureSystemUserSession(token);
            });

            return session;
        }

        public static User Ensure(UserInfo user, TokenInfo token)
        {
            return Db.Transact(() =>
            {
                // See if we find an existing OAuthUser
                var oauthUser = Db.SQL<User>("SELECT o FROM OAuth2.Core.Database.User o WHERE o.Provider=? and o.Id=?", user.Provider, user.ProviderId).First;

                if (null == oauthUser)
                {
                    oauthUser = new User()
                    {
                        Provider = user.Provider,
                        Id = user.ProviderId,
                        Name = user.Name,
                        Email = user.Email,
                        Link = user.Profile,
                        Picture = user.Picture,
                        Locale = user.Locale,
                        Token = new Token()
                    };
                }

                oauthUser.Token.AccessToken = token.AccessToken;
                oauthUser.Token.RefreshToken = token.RefreshToken;
                oauthUser.Token.Expires = token.Expires;
                oauthUser.Token.Issued = token.Issued;

                SystemUser currentUser = SystemUser.GetCurrentSystemUser();

                if (null == oauthUser.SystemUser)
                {
                    if (null == currentUser)
                    {
                        // JIT-create a SystemUser & populate the empty Person to it
                        currentUser = SystemUser.RegisterSystemUser(user.Provider + ":" + user.ProviderId, user.Email, RandomString(32));
                        Person person = (Person)currentUser.WhoIs;

                        person.FirstName = user.FirstName;
                        person.LastName = user.LastName;

                        if (null!=user.Picture)
                        {
                            new Illustration()
                            {
                                Content = new Content()
                                {
                                    URL = user.Picture,
                                    MimeType = MimeMapping.GetMimeMapping(user.Picture)
                                },
                                Concept = person
                            };
                        }
                    }

                    oauthUser.SystemUser = currentUser;
                }

                if (null == currentUser)
                    currentUser = oauthUser.SystemUser;

                if (oauthUser.SystemUser.Username!=currentUser.Username)
                {
                    // remove dangling systemuser previously related to this oauthUser
                    // (if there are _no other_ oauthUser's still related to it)
                    if(1 == Db.SQL<User>("SELECT o FROM OAuth2.Core.Database.User o WHERE o.SystemUser=?", oauthUser.SystemUser).Count())
                    {
                        var su = Db.SQL<SystemUser>("SELECT su FROM Simplified.Ring3.SystemUser su WHERE su.ObjectNo=?", oauthUser.SystemUser.GetObjectNo()).First;

                        if(null!=su)
                            ((Person)su.WhoIs).Deleted = true; // Should we delete person? Or leave "?as is"?

                        Db.SQL<SystemUser>("DELETE FROM Simplified.Ring3.SystemUser WHERE ObjectNo=?", oauthUser.SystemUser.GetObjectNo());
                    }
                }

                // relate this oauthuser to currentuser
                oauthUser.SystemUser = currentUser;

                return oauthUser;
            });
        }

        protected static string RandomString(int size)
        {
            string input = "abcdefghijklmnopqrstuvwxyz0123456789";
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;

            for (int i = 0; i < size; i++)
            {
                ch = input[random.Next(0, input.Length)];
                builder.Append(ch);
            }

            return builder.ToString();
        }

        protected static String CreateAuthToken(string userid)
        {
            // Server has a secret key K (a sequence of, say, 128 bits, produced by a cryptographically secure PRNG).
            // A token contains the user name (U), the time of issuance (T), and a keyed integrity check computed over U and T (together),
            // keyed with K (by default, use HMAC with SHA-256 or SHA-1).
            // Auth token    Username+tokendate
            byte[] randomNumber = new byte[16];
            RNGCryptoServiceProvider rngCsp = new RNGCryptoServiceProvider();

            rngCsp.GetBytes(randomNumber);

            return HttpServerUtility.UrlTokenEncode(randomNumber);
        }

        protected static SystemUserSession AssureSystemUserSession(SystemUserTokenKey Token)
        {
            SystemUserSession session = GetCurrentSystemUserSession();

            if (session == null)
            {
                session = new SystemUserSession();
                session.Created = DateTime.UtcNow;
                session.SessionIdString = Session.Current.SessionId;
            }

            session.Token = Token;
            session.Touched = DateTime.UtcNow;

            return session;

        }
    }
}
