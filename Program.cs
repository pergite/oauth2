﻿using System;
using Starcounter;
using System.Collections.Generic;

using Simplified.Ring3;

using OAuth2.Core.Services;
using OAuth2.Core.Helpers;
using OAuth2.Core.Models;

using OAuth2.ViewModels;
using OAuth2.Helpers;
using System.Web;
using System.Collections.Specialized;

namespace OAuth2
{
    class Program
    {
        protected const string AUTH_COOKIE_NAME = "soauthtoken";

        static void Main()
        {
            Application.Current.Use(new HtmlFromJsonProvider());
            Application.Current.Use(new PartialToStandaloneHtmlProvider());

            AuthorizationHelper.EnsurePermissions();


            Func<string, string, Request, Response> doSignin = (string serviceName, string queryString, Request request) =>
            {
                ObSession session = SessionManager.NewSession();

                ServiceBase p = Factory.Get(serviceName);

                if (p == null)
                    return Response.FromStatusCode(404);

                List<string> scope = new List<string>();

                if(!String.IsNullOrEmpty(queryString))
                {
                    NameValueCollection query = HttpUtility.ParseQueryString(queryString);

                    if(null != query["scope"])
                    {
                        scope.Add(query["scope"]);

                        session["scope"] = scope;
                    }

                    if(null != query["final_destination"])
                    {
                        session["final_destination"] = HttpUtility.UrlDecode(query["final_destination"]);
                    }
                }

                var response = ResponseHelper.Redirect(p.GetAuthEndpoint(session, request.MakeAbsoluteUrl("/oauth2/{0}/callback", serviceName), "", scope));

                return SessionManager.SetOnResponse(response, session);
            };

            Handle.GET("/oauth2/{?}/signin?{?}", doSignin);
            Handle.GET("/oauth2/{?}/signin", (string serviceName, Request request) =>
            {
                return doSignin(serviceName, null, request);
            });

            Handle.GET("/oauth2/{?}/callback?{?}", (string serviceName, string queryString, Request request) => {

                ObSession session = SessionManager.GetFromRequest(request);

                if(null==session)
                {
                    // No session, probably too long since signin called "redirect" back to signinpage
                    // Re-run the oauth signin step
                    return Self.GET(String.Format("/oauth2/{0}/signin", serviceName));
                }

                var scope = session["scope"] as ICollection<string>;

                ServiceBase p = Factory.Get(serviceName);
                AuthResponseResult result = p.ProcessAuthResponse(session, queryString, request.MakeAbsoluteUrl("/oauth2/{0}/callback", serviceName), "", scope);

                if (!result)
                {
                    if(result.ShouldReRunAuthentication && !session.ContainsKey("rerun"))
                    {
                        session["rerun"] = true;
                        var response = ResponseHelper.Redirect(p.GetAuthEndpoint(session, request.MakeAbsoluteUrl("/oauth2/{0}/callback", serviceName), "", scope, result.ReRunAuthentication.AdditionalRequestParameters));

                        return SessionManager.SetOnResponse(response, session);
                    }

                    return Response.FromStatusCode(401);
                }
                    

                // ensure OAuthUser && SystemUser
                var oauthUser = UserHelper.Ensure(result.User, result.Token);

                // log us in (if needed)
                if (null == SystemUser.GetCurrentSystemUser())
                {
                    // ensure that we have a valid SCSession
                    Session.Ensure();

                    var scsession = UserHelper.SignIn(oauthUser.SystemUser);

                    Cookie cookie = new Cookie(AUTH_COOKIE_NAME, scsession.Token.Token);

                    Handle.AddOutgoingCookie(cookie.Name, cookie.GetFullValueString());
                }

                string final_destination = session["final_destination"] as string;
                if(!String.IsNullOrEmpty(final_destination))
                {
                    //return ResponseHelper.Redirect(final_destination);
                    var redirectPage = new RedirectPage
                    {
                        RedirectUrl = final_destination
                    };

                    return redirectPage;
                }

                return ResponseHelper.Redirect("/signin");
            });

            Func<string, Request, Response> handleSignin = (string finalDestination, Request request) => 
            {
                var signinPage = new SignInPage
                {
                    Data = SettingsHelper.GetSettings(),
                    FinalDestination = HttpUtility.UrlEncode(HttpUtility.UrlDecode(finalDestination))
                };

                return signinPage;
            };

            Handle.GET("/oauth2/signin?{?}", handleSignin);
            Handle.GET("/oauth2/signin", (Request request) => { return handleSignin(null, request); });

            Handle.GET("/oauth2/add", (Request request) => { return handleSignin(null, request); });
            Handle.GET("/oauth2/add?{?}", handleSignin);

            Handle.GET("/oauth2/admin/settings", (Request request) =>
            {
                Json page;
                if (!AuthorizationHelper.TryNavigateTo("/signin/admin/settings", request, out page))
                {
                    return page;
                }

                return Db.Scope(() =>
                {
                    var settingsPage = new SettingsPage
                    {
                        Data = SettingsHelper.GetSettings()
                    };
                    return settingsPage;
                });
            });

            Blender.MapUri("/oauth2/signin", "user", null);
            Blender.MapUri("/oauth2/signin", "userform", null);
            Blender.MapUri("/oauth2/signin?{?}", "userform-return", null);
        }
    }
}
