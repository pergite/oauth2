using Starcounter;

namespace OAuth2.ViewModels
{
    partial class SettingsPage : Json
    {
        void Handle(Input.Save action)
        {
            if (this.Transaction.IsDirty)
            {
                this.Transaction.Commit();
            }

            this.SuccessMessage = "Changes saved";
        }

        void Handle(Input.Close action)
        {
            if (this.Transaction.IsDirty)
            {
                this.Transaction.Rollback();
            }

            this.RedirectUrl = "/signin/profile";
        }
    }
}
